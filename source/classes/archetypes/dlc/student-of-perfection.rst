.. include:: /helpers/roles.rst

.. rst-class:: archetype
.. _archetype--Student-of-Perfection:

Ученик Совершенства (`Student of Perfection <https://2e.aonprd.com/Archetypes.aspx?ID=18>`_)
-------------------------------------------------------------------------------------------------------------

Вы учились боевым искусствам в "Домах совершенства" Джалмерэя.


.. _arch-feat--Student-of-Perfection--Dedication:

Посвящение ученика совершенства (`Student of Perfection Dedication <https://2e.aonprd.com/Feats.aspx?ID=900>`_) / 2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- :uncommon:`необычный`
- архетип
- посвящение

**Предварительные условия**: :ref:`class-feature--Monk--Powerful-Fist` или эксперт безоружных атак; член "Дома Совершенства"

**Доступ**: Вы из Джалмерэя

**Источник**: Lost Omens: World Guide pg. 83

----------

Вы научились техникам боевых искусств своего "Дома Совершенства".
Вы становитесь обучены Акробатике или Атлетике на свой выбор, и Знаниям военного дела (Warfare); если вы уже обучены, то вместо этого становитесь экспертом.
Вы получаете классовую способность монаха :ref:`class-feat--monk--Ki-Strike`, которая дает вам заклинание ки :ref:`spell--focus--Ki-Strike` и запас очков фокусировки равный 1, которое вы можете восстановить при использовании :ref:`action--Refocus` как это делает монах.
Ваши заклинания ки "Ученика Совершенства" являются оккультными заклинаниями.

**Особенность**: Вы не можете выбрать другую способность посвящения, пока не получите 2 другие способности из архетипа ученик совершенства.


.. _arch-feat--Student-of-Perfection--Perfect-Strike:

Совершенный удар (`Perfect Strike <https://2e.aonprd.com/Feats.aspx?ID=901>`_) / 4
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- архетип

**Предварительные условия**: :ref:`arch-feat--Student-of-Perfection--Dedication`

**Источник**: Lost Omens: World Guide pg. 83

----------

Вы получаете заклинание ки :ref:`spell--focus--Perfect-Strike`.
Увеличьте запас Очков Фокусировки на 1.


.. _arch-feat--Student-of-Perfection--Perfect-Weaponry:

Совершенное оружие (`Perfect Weaponry <https://2e.aonprd.com/Feats.aspx?ID=4059>`_) / 4
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- архетип

**Предварительные условия**: :ref:`arch-feat--Student-of-Perfection--Dedication`

**Источник**: Lost Omens: Impossible Lands pg. 222

----------

Вы получаете классовую способность монаха :ref:`class-feat--monk--Monastic-Weaponry`.
Вы можете использовать оружие ближнего боя с признаком :t_monk:`монах` с любыми способностями или заклинаниями фокусировки Ученика Совершенства, для которых обычно требуются безоружные атаки.


.. _arch-feat--Student-of-Perfection--Perfect-Ki-Adept:

Адепт совершенного ки (`Perfect Ki Adept <https://2e.aonprd.com/Feats.aspx?ID=902>`_) / 6
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- архетип

**Предварительные условия**: :ref:`arch-feat--Student-of-Perfection--Dedication`

**Источник**: Lost Omens: World Guide pg. 83

----------

Вы получаете соответствующее заклинание ки вашей "Школы Совершенства":

* :ref:`spell--focus--Unblinking-Flame-Revelation` для "Вечного пламени" (Unblinking Flame),
* :ref:`spell--focus--Unbreaking-Wave-Advance` для "Несокрушимых волн" (Unbreaking Waves),
* :ref:`spell--focus--Unfolding-Wind-Rush` для "Несворачивающего ветра" (Unfolding Wind),
* :ref:`spell--focus--Untwisting-Iron-Buffer` для "Несгибаемого железа" (Untwisting Iron)

Увеличьте запас Очков Фокусировки на 1.


.. _arch-feat--Student-of-Perfection--Perfect-Resistance:

Совершенное сопротивление (`Perfect Resistance <https://2e.aonprd.com/Feats.aspx?ID=4060>`_) / 8
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- архетип

**Предварительные условия**: :ref:`arch-feat--Student-of-Perfection--Dedication`

**Источник**: Lost Omens: Impossible Lands pg. 222

----------

Вы получаете сопротивление, равное половине вашего уровня ко всему урону от эффектов с признаком, соответствующим вашей Школе Совершенства: :t_fire:`огонь` для "Вечного пламени" (Unblinking Fire), :t_water:`вода` для "Несокрушимых волн" (Unbreakable Waves), :t_air:`воздух` для "Несворачивающего ветра" (Unfolding Wind) и :t_earth:`земля` для "Несгибаемого железа" (Untwisting Iron).
После использования заклинания ки Ученика Совершенства, на 1 раунд это сопротивление увеличивается до значения равного вашему уровню.


.. _arch-feat--Student-of-Perfection--Perfect--Ki-Expert:

Эксперт совершенного ки (`Perfect Ki Expert <https://2e.aonprd.com/Feats.aspx?ID=4061>`_) / 10
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- архетип

**Предварительные условия**: :ref:`arch-feat--Student-of-Perfection--Dedication`

**Источник**: Lost Omens: Impossible Lands pg. 222

----------

Вы получаете соответствующее заклинание ки вашей "Школы Совершенства":

* :ref:`spell--focus--Unblinking-Flame-Aura` для "Вечного пламени" (Unblinking Flame),
* :ref:`spell--focus--Unbreaking-Wave-Vapor` для "Несокрушимых волн" (Unbreaking Waves),
* :ref:`spell--focus--Unfolding-Wind-Buffet` для "Несворачивающего ветра" (Unfolding Wind),
* :ref:`spell--focus--Untwisting-Iron-Roots` для "Несгибаемого железа" (Untwisting Iron)

Увеличьте запас Очков Фокусировки на 1.
Вы становитесь экспертом атак оккультными заклинаниями и КС оккультных заклинаний.


.. _arch-feat--Student-of-Perfection--Perfect--Ki-Exemplar:

Образец совершенного ки (`Perfect Ki Exemplar <https://2e.aonprd.com/Feats.aspx?ID=4062>`_) / 14
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- архетип

**Предварительные условия**: :ref:`arch-feat--Student-of-Perfection--Dedication`

**Источник**: Lost Omens: Impossible Lands pg. 222

----------

Вы получаете соответствующее заклинание ки вашей "Школы Совершенства":

* :ref:`spell--focus--Unblinking-Flame-Emblem` для "Вечного пламени" (Unblinking Flame),
* :ref:`spell--focus--Unbreaking-Wave-Barrier` для "Несокрушимых волн" (Unbreaking Waves),
* :ref:`spell--focus--Unfolding-Wind-Blitz` для "Несворачивающего ветра" (Unfolding Wind),
* :ref:`spell--focus--Untwisting-Iron-Augmentation` для "Несгибаемого железа" (Untwisting Iron)

Увеличьте запас Очков Фокусировки на 1.
Вы становитесь экспертом атак оккультными заклинаниями и КС оккультных заклинаний.


.. _arch-feat--Student-of-Perfection--Perfect--Ki-Grandmaster:

Магистр совершенного ки (`Perfect Ki Grandmaster <https://2e.aonprd.com/Feats.aspx?ID=4063>`_) / 18
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- архетип

**Предварительные условия**: :ref:`arch-feat--Student-of-Perfection--Perfect--Ki-Expert` или :ref:`arch-feat--Student-of-Perfection--Perfect--Ki-Exemplar`

**Источник**: Lost Omens: Impossible Lands pg. 222

----------

Вы получаете соответствующее заклинание ки вашей "Школы Совершенства":

* :ref:`spell--focus--Unblinking-Flame-Ignition` для "Вечного пламени" (Unblinking Flame),
* :ref:`spell--focus--Unbreaking-Wave-Containment` для "Несокрушимых волн" (Unbreaking Waves),
* :ref:`spell--focus--Unfolding-Wind-Crash` для "Несворачивающего ветра" (Unfolding Wind),
* :ref:`spell--focus--Untwisting-Iron-Pillar` для "Несгибаемого железа" (Untwisting Iron)

Увеличьте запас Очков Фокусировки на 1.
Вы становитесь мастером атак оккультными заклинаниями и КС оккультных заклинаний.





.. include:: /helpers/actions.rst