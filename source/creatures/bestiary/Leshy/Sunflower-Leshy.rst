.. include:: /helpers/roles.rst


.. rst-class:: creature-details

Подсолнечный леший (Sunflower Leshy)
============================================================================================================

Подсолнечные лешие - послы и социальные лидеры своих сородичей.
Их лепестки расходятся от головы похожим на бороду, лиственным, зубчатым ершиком и имеют разные оттенки.
Когда они особенно взбудораженны, их глаза и лепестки светятся собранным солнечным светом, а когда они подавлены или грустят, их обычно яркая окраска становится соответственно приглушенной.
Эти изменения окраски отражаются в искусстве подсолнечных леших, что может привести к неожиданной цветовой палитре произведений искусства.
Например, человеческий художник может изобразить :ref:`Ад <plane--Hell>` как место раскаленной лавы и мерцающего пламени, используя множество ярких цветов, но для подсолнечного лешего единственным правильным способом изобразить такое место, как Ад, являются угрюмые серые, черные и белые цвета.

Более отважные подсолнечные лешие покидают свои общины, чтобы наладить связи с близлежащими поселениями гуманоидов.
При общении с гуманоидами подсолнечные лешие больше других склонны к дружелюбию и дают незнакомцам шанс доказать свои добрые намерения, хотя и остаются осторожными.
Зачастую их сопровождают другие, более могущественные лешие.



.. rst-class:: creature
.. _bestiary--Sunflower-Leshy:

Подсолнечный леший (`Sunflower Leshy <https://2e.aonprd.com/Monsters.aspx?ID=716>`_) / Существо 1
------------------------------------------------------------------------------------------------------------

- :alignment:`Н`
- :size:`маленький`
- леший
- растение

**Источник**: Bestiary 2 pg. 160

**Восприятие**: +7;
:ref:`ночное зрение <cr_ability--Darkvision>`

**Языки**: Всеобщий,
Друидский,
Сильван;
:ref:`spell--s--Speak-with-Plants` (только подсолнухи)

**Навыки**:
Акробатика +6,
Скрытность +7 (+9 на равнинах),
Дипломатия +8,
Природа +5

**Сил** +0,
**Лвк** +3,
**Тел** +1,
**Инт** +0,
**Мдр** +2,
**Хар** +3

----------

**КБ**: 16;
**Стойкость**: +4,
**Рефлекс**: +10,
**Воля**: +7

**ОЗ**: 20


**Гелиотроп (Heliotrope)** |д-св|
(:t_aura:`аура`, :t_light:`свет`, :t_primal:`природный`, :t_evocation:`эвокация`)
20 футов;
**Требования**: Подсолнечный леший начинает свой ход в области яркого света;
**Эффект**: Подсолнечный леший отражает от своего лица солнце или другой источник яркого света.
Каждое существо, завершающее свой ход в этой ауре, должно совершить спасбросок Воли КС 16.

| **Успех**: Существо невредимо и временно иммунно к гелиотропу на 24 часа.
| **Провал**: Существо отвлечено светом, становясь :c_flat_footed:`застигнутым врасплох` на 1 раунд.
| **Критический провал**: Как провал, но существо еще :c_dazzled:`ослеплено` на 1 раунд.


**Зеленый взрыв (Verdant Burst)**
(:t_healing:`исцеление`)
Когда подсолнечный леший умирает, его тело разрывается взрывом природной энергии, восстанавливая 1d8 ОЗ всем :t_plant:`растительным` существам в 30-футовой эманации.
Эта область заполнена подсолнухами, становясь сложной местностью.
Если для этих растений данная местность не является приемлемым окружением, то они увядаю через 24 часа.

----------

**Скорость**: 25 футов


**Ближний бой**: |д-1| завиток +6 [+2/-2] (:w_agile:`быстрое`, :w_finesse:`точное`),
**Урон** 1d8 дробящий


**Дальний бой**: |д-1| семечко +6 [+1/-4] (шаг дистанции 20 фт),
**Урон** 1d6 дробящий


**Разброс семян (Seed Spray)** |д-2|
(:t_primal:`природный`, :t_conjuration:`воплощение`)
Подсолнечный леший выпускает из головы поток семян 15-футовым конусом, нанося существам в области 2d6 дробящего урона (простой спасбросок Рефлекса КС 16).
Против :c_dazzled:`ослепленных` существ он получает к этому урону бонус состояния +2.
Он не может снова использовать "Разброс семян" 1d4 раунда.


**Врожденные природные заклинания** КС 17

| **4 ур.** :ref:`spell--s--Speak-With-Plants`


:ref:`cr_ability--Change-Shape` |д-1|
(:t_primal:`природный`, :t_transmutation:`трансмутация`, :t_polymorph:`полиморф`, :t_concentrate:`концентрация`)
Подсолнечный леший может превращаться в :s_small:`маленький` цветок.
В остальном, это умение использует эффекты :ref:`spell--t--Tree-Shape`.





.. include:: /helpers/actions.rst