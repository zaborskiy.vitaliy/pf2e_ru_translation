.. include:: /helpers/roles.rst

.. rst-class:: creature-details

***********************************************************************************************************
Скельм (`Skelm <https://2e.aonprd.com/MonsterFamilies.aspx?ID=260>`_)
***********************************************************************************************************

**Источник**: Bestiary 3 pg. 238

Охваченных яростью скельмов привлекает любое поселение, в котором больше нескольких сотен душ.
Применяя магическую маскировку и используя общественные нормы в своих интересах, эти рогатые монстры жаждут уважения из-за страха и жестоко карают любого, кто осмеливается не соглашаться с их возвышенными мнениями даже в малейшей степени.
Хотя скельмы весьма опасны сами по себе, они наиболее смертоносны, когда возглавляют разъяренную толпу.
Их жестокая и эксплуататорская натура сделала их имя синонимом злодейства.

Скельм может превратить в одного из своих собратьев любого злого гуманоида, которого переполняет ярость.
В качестве идеологической обработки, скельмы подвергают потенциальных новых собратьев гневным унижениям, убеждая новобранцев, что за их несчастья ответственен какой-то другой человек или группа.
Эта практика гарантирует, что скельмы начинают свое новое существование с достаточной яростью, чтобы планировать свою месть.

Многие новообразованные скельмы продолжают свою жизнь в тех ролях, которые они исполняли, будучи простыми смертными; более того, эти роли часто определяют вид скельма, которым они становятся.
Скельмы могут возникнуть среди представителей практически любой родословной, но чаще всего они встречаются в культурах с глубоко укоренившимися гендерными ролями, несправедливыми иерархиями, а также там, где нет здоровых способов испытывать и переживать гнев.



.. toctree::
   :glob:
   :maxdepth: 3
   
   *



.. rst-class:: h3
.. rubric:: Насмешки над мужественностью (Mockeries of Masculinity)

|creatures|

**Источник**: Bestiary 3 pg. 238

Похоже, что женщин скельмов не существует.
Те немногие исследователи, которые проследили брачное поведение скельмов, обнаружили, что их потомство - это недифференцированные представители материнской родословной.
Тот факт, что скельмы являются жестокими оборотнями-манипуляторами, но все они мужчины, привел к теории, что скельмы - мистический мужской аналог :doc:`карг </creatures/bestiary/Hag/index>`.


.. rst-class:: h3
.. rubric:: Рога скельмов (Skelm Antlers)

|lore|

**Источник**: Bestiary 3 pg. 239

У всех скельмов ветвистые рога, напоминающие оленьи.
Скельмы с меньшими рогами стыдят и задирают скельмов с большими рогами, хотя всегда используют какое-нибудь другое оправдание.
Однако, когда они имеют дело с не-скельмами, то делают вид, что у них вообще нет рогов, независимо от доказательств или аргументов.
Скельмы даже потрошат врагов своими рогами в бою, даже если после этого они отнекиваются от только что совершенного действия.


.. rst-class:: h3
.. rubric:: Общество скельмов (Skelm Society)

|lore|

**Источник**: Bestiary 3 pg. 240

Альянсы между скельмами редко длятся долго, ведь это лишь вопрос времени, когда один из них ранит невероятно хрупкое эго другого, разрушая союз.
Многие образуют иерархические клубы со смертными представителями, чтобы отсрочить такой конфликт и одновременно выявить перспективных новых скельмов.


.. rst-class:: h3
.. rubric:: Происхождение скельмов (Skelm Origins)

|lore|

**Источник**: Bestiary 3 pg. 241

Превращение из мужчины в скельма происходит в течение удивительно короткого периода времени - иногда всего за несколько часов - поскольку физические изменения относительно незначительны по сравнению с требуемой эмоциональной преданностью.





.. include:: /helpers/actions.rst
.. include:: /helpers/bestiary-icons.rst