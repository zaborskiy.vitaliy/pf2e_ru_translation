.. include:: /helpers/roles.rst


.. rst-class:: creature-details

Дракси (Draxie)
============================================================================================================

.. sidebar:: |creatures|

	.. rubric:: Элукрея (Elucrea)
	
	**Источник**: Bestiary 3 pg. 255
	
	Исключением из взбалмошной природы дракси является элукрея - пожизненная связь, образующаяся между дракси и существом или существами, которых дракси особенно любит, обычно с теми у кого хорошее чувство юмора.
	Согласно драксийской легенде, частичка духа дракси до сих пор помнит, как их объединял древний :doc:`волшебный дракончик </creatures/bestiary/Faerie-Dragon>` Элукредасса, и это заставляет дракси жаждать подобных связей с другими.


Озорные драконьи спрайты, известные как дракси, на протяжении веков соперничали со своими кузенами :doc:`пикси <Pixie>` за звание величайшего проказника.
Обладая драконьими чертами и яркой окраской, дракси происходят от могущественного :doc:`волшебного дракончика </creatures/bestiary/Faerie-Dragon>`, который, в соответствии с переменчивой манерой царства фей, после своей смерти реинкарнировал в нескольких дракси.
Дракси проявляют терпение и планирование, чтобы устраивать идеальные розыгрыши, тратя на свои усилия месяцы, а то и годы.



.. rst-class:: creature
.. _bestiary--Draxie:

Дракси (`Draxie <https://2e.aonprd.com/Monsters.aspx?ID=1323>`_) / Существо 3
------------------------------------------------------------------------------------------------------------

- :alignment:`ХД`
- :size:`крошечный`
- спрайт
- фея

**Источник**: Bestiary 3 pg. 255

**Восприятие**: +8;
:ref:`сумеречное зрение <cr_ability--Low-Light-Vision>`

**Языки**: Всеобщий,
Сильван;
:ref:`телепатия <cr_ability--Telepathy>` (касание)

**Навыки**:
Акробатика +9,
Обман +10,
Скрытность +11,
Дипломатия +8,
Природа +6

**Сил** -1,
**Лвк** +4,
**Тел** +1,
**Инт** +3,
**Мдр** +1,
**Хар** +3

----------

**КБ**: 19;
**Стойкость**: +6,
**Рефлекс**: +11,
**Воля**: +8

**ОЗ**: 45

**Слабости**: :ref:`холодное железо <material--Cold-Iron>` 5

----------

**Скорость**: 15 футов,
полет 40 футов


**Ближний бой**: |д-1| челюсти +11 [+7/+3] (:w_agile:`быстрое`, :w_finesse:`точное`, :t_magical:`магический`),
**Урон** 1d8+3 дробящий


**Дальний бой**: |д-1| искра эйфории +7 [+2/-3] (дистанция 20 фт, :t_enchantment:`очарование`, :t_magical:`магический`),
**Урон** 2d4+3 ментальный



**Врожденные природные заклинания** КС 20

| **2 ур.**
| :ref:`spell--f--Faerie-Fire`
| :ref:`spell--i--Invisibility`
| **1 ур.** :ref:`spell--i--Illusory-Disguise` (×3)
| **Чары (1 ур.)**
| :ref:`spell--d--Dancing-Lights`
| :ref:`spell--g--Ghost-Sound`
| :ref:`spell--p--Prestidigitation`


**Атака дыханием (Breath Weapon)** |д-2|
(:t_incapacitation:`недееспособность`, :t_primal:`природный`, :t_enchantment:`очарование`, :t_emotion:`эмоция`, :t_mental:`ментальный`)
Дракси выдыхает 15-футовым конусом пыль пикси со случайным эффектом, который определяется каждый раз при использовании "Атаки дыханием".
Каждое существо в этой области должно совершить успешный спасбросок Воли КС 17, иначе становится подверженно эффекту.
Киньте 1d4 для определения эффекта.
Он не может снова использовать "Атаку дыханием" 1d4 раунда.

1. Цель подвергается эффектам заклинания :ref:`spell--c--Charm`.
2. Цель теряет последние 5 минут воспоминаний.
3. Цель подвергается эффектам заклинания :ref:`spell--s--Sleep`.
4. Цель становится :c_stupefied:`одурманена 2` и :c_slowed:`замедлена 1` от эйфории.





.. include:: /helpers/actions.rst
.. include:: /helpers/bestiary-icons.rst