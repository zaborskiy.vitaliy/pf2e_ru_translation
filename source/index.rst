.. Pathfinder 2 Core Rulebook documentation master file, created by
   sphinx-quickstart on Wed Sep 18 11:06:43 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

| **ВАЖНЫЕ ОБНОВЛЕНИЯ | 2023-08-08**
| Добавлен класс **кинетик** из :ref:`Rage of Elements <changelog--RoE--2023-08-08>`

| **СООБЩЕСТВО**
| `Discord сервер <https://discord.gg/FxwzjtX3mh>`_ | `Telegram канал <https://t.me/PF2e_RU_channel>`_ | `Telegram чат <https://t.me/+UuDJ_hPEOGoySmY3>`_
| Если вы хотите поддержать проект, то можете найти информацию на странице :doc:`support`.


Pathfinder 2E
======================================================

.. toctree::
   :maxdepth: 2
   
   introduction
   ancestries_and_backgrounds/index
   classes/index
   skills
   feats
   equipment/index
   spells/index
   lost_omens/index
   playing_the_game
   game_mastering/index
   crafting_and_treasure/index
   creatures/index
   books/index
   appendix/index


.. toctree::
   :maxdepth: 1

   licenses
   support
   changelog
