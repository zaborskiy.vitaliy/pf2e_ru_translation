.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--Entwined-Roots:

Переплетенные корни (`Entwined Roots <https://2e.aonprd.com/Spells.aspx?ID=1400>`_) / Закл. 5
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- дерево
- концентрация
- воздействие

**Обычай**: арканный, природный

**Сотворение**: 1 минута

**Область**: 20-футовый взрыв

**Цели**: вплоть до 5 готовых живых существ

**Продолжительность**: 10 минут

**Источник**: Rage of Elements pg. 196

----------

Ползучие корни опоясывают вас и ваших союзников слоями гибкой деревянной защиты.
Каждое подверженное эффекту существо получает сопротивление 5 дробящему и колющему урону.
Когда существо, защищенное этими корнями, становится целью атаки дистанционным оружием, но не получает урон (например, если атака промахнулась или урон был снижен до 0 из-за сопротивления), корни подхватывают амуницию или :w_thrown:`метательное` оружие и удерживают его.
Защищенное существо может достать это брошенное оружие или амуницию действием :ref:`action--Interact`. 

----------

**Усиление (7-й)**: Сопротивление равно 10.

**Усиление (9-й)**: Сопротивление равно 15.





.. include:: /helpers/actions.rst