.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--s--Soothe:

Успокоение (`Soothe <https://2e.aonprd.com/Spells.aspx?ID=291>`_) / Закл. 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- очарование
- исцеление
- эмоция
- ментальный

**Обычай**: оккультный

**Сотворение**: |д-2| жестовый, словесный

**Дистанция**: 30 футов

**Цели**: 1 готовое существо

**Продолжительность**: 1 минута

**Божества**:
:doc:`/lost_omens/Deity/Other/Tsukiyo`,
:doc:`/lost_omens/Deity/Eldest/The-Lost-Prince`,
:doc:`/lost_omens/Deity/Elemental-Lord/Atreia`,
:doc:`/lost_omens/Deity/Empyreal-Lord/Ashava`,
:doc:`/lost_omens/Deity/Empyreal-Lord/Korada`,
:doc:`/lost_omens/Deity/Monitor-Demigod/Mother-Vulture`,
:doc:`/lost_omens/Deity/Dwarven-God/Bolka`,
:doc:`/lost_omens/Deity/Other/More/Naderi`,
:doc:`/lost_omens/Deity/Old-Sun-God/Tlehar`,
:doc:`/lost_omens/Deity/Tian-God/Kofusachi`,
:doc:`/lost_omens/Deity/Tian-God/Qi-Zhong`,
:doc:`/lost_omens/Deity/Ancient-Osirian-God/Isis`,
:doc:`/lost_omens/Deity/Ancient-Osirian-God/Osiris`,
:doc:`/lost_omens/Deity/Ancient-Osirian-God/Selket`

**Пантеоны**:
:doc:`/lost_omens/Deity/Pantheon/Wards-of-the-Pharaoh`,
:doc:`/lost_omens/Deity/Pantheon/Touch-of-the-Sun`

----------

Вы успокаиваете разум цели, повышая ее ментальную защиту и исцеляя ее раны.
Цель восстанавливает 1d10+4 ОЗ когда вы совершаете :ref:`action--Cast-a-Spell` и получает бонус состояния +2 к спасброскам против :t_mental:`ментальных` эффектов в течение продолжительности.

----------

**Усиление (+1)**: Количество исцеления увеличивается на 1d10+4.

.. versionchanged:: /errata-CR-r4
	Из записи о целях убрано "живое". Заклинание можно использовать для исцеления :t_undead:`нежити`, :t_construct:`конструктов` и т.п. Что приводит его в соответствие со сноской из "Книги мертвых" - :ref:`Исцеление нежити (Healing Undead) <sidebar--BotD--Healing-Undead>`. Но стоит обратить внимание, что из-за признака :t_mental:`ментальный`, оно все еще не может исцелять или давать преимущества :t_mindless:`неразумным` существам, таким как многие :doc:`зомби </creatures/bestiary/Zombie/index>` или :doc:`оживленные объекты </creatures/bestiary/Animated-Object/index>`.





.. include:: /helpers/actions.rst